package tcp

import (
	"context"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"codeberg.org/lsp/in-depth-go-principles/go-redis/interface/tcp"
	"codeberg.org/lsp/in-depth-go-principles/go-redis/lib/logger"
)

type Config struct {
	Address string
}

func ListenAndServeWithSignal(cfg *Config, handler tcp.Handler) error {
	closeChan := make(chan struct{})
	// 监听系统信号
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		s := <-sigChan
		switch s {
		case syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGINT:
			closeChan <- struct{}{}
		}

	}()

	listen, err := net.Listen("tcp", cfg.Address)
	if err != nil {
		return err
	}

	logger.Info("start listen")

	ListenAndServe(listen, handler, closeChan)

	return nil
}

func ListenAndServe(listen net.Listener, handler tcp.Handler, closeChan <-chan struct{}) {
	closer := func() {
		handler.Close()
		listen.Close()
	}

	go func() {
		<-closeChan
		logger.Info("shutting down")
		closer()
	}() // 接收信号关闭，释放资源

	defer closer() // 正常关闭，释放资源

	ctx := context.Background()
	wg := sync.WaitGroup{} // 等待所有已存在的连接处理完成

	for {
		conn, err := listen.Accept()
		if err != nil {
			break
		}
		logger.Info("accepted link")
		wg.Add(1)
		go func() {
			defer wg.Done()
			handler.Handle(ctx, conn)
		}()
	}

	wg.Wait()
}
