package tcp

import (
	"bufio"
	"context"
	"io"
	"net"
	"sync"
	"time"

	"codeberg.org/lsp/in-depth-go-principles/go-redis/lib/logger"
	"codeberg.org/lsp/in-depth-go-principles/go-redis/lib/sync/atomic"
	"codeberg.org/lsp/in-depth-go-principles/go-redis/lib/sync/wait"
)

type EchoClient struct {
	Conn    net.Conn
	Watting wait.Wait
}

func (c *EchoClient) Close() error {
	c.Watting.WaitWithTimeout(10 * time.Second)
	return c.Conn.Close()
}

type EchoHandler struct {
	activeConn sync.Map
	closing    atomic.Boolean
}

func (h *EchoHandler) Handle(ctx context.Context, conn net.Conn) {
	if h.closing.Get() {
		conn.Close()
	}
	client := &EchoClient{
		Conn: conn,
	}
	h.activeConn.Store(client, struct{}{})

	reader := bufio.NewReader(conn)
	for {
		msg, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				logger.Info("connection closed")
				h.activeConn.Delete(client)
			} else {
				logger.Warn(err)
			}
			return
		}
		client.Watting.Add(1)
		conn.Write([]byte(msg))
		client.Watting.Done()
	}
}
func (h *EchoHandler) Close() error {
	logger.Info("handler shutting down")
	h.closing.Set(true)
	h.activeConn.Range(func(key, value any) bool {
		cli, ok := key.(*EchoClient)
		if ok {
			cli.Conn.Close()
		}
		return true
	})
	return nil
}
