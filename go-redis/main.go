package main

import (
	"os"

	"codeberg.org/lsp/in-depth-go-principles/go-redis/config"
	"codeberg.org/lsp/in-depth-go-principles/go-redis/lib/logger"
	"codeberg.org/lsp/in-depth-go-principles/go-redis/tcp"
)

const configFile string = "redis.conf"

var defaultProperties = &config.ServerProperties{
	Bind: "0.0.0.0",
	Port: 6379,
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	return err == nil && !info.IsDir()
}

func main() {
	logger.Setup(&logger.Settings{
		Path:       "logs",
		Name:       "go-redis",
		Ext:        "log",
		TimeFormat: "2006-01-02",
	})

	if fileExists(configFile) {
		config.SetupConfig(configFile)
	} else {
		config.Properties = defaultProperties
	}

	if err := tcp.ListenAndServeWithSignal(
		&tcp.Config{Address: config.Properties.Address()},
		&tcp.EchoHandler{}); err != nil {
		logger.Fatal()
	}
}
