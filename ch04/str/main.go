package main

import (
	"fmt"
	"reflect"
	"unsafe"
)

func main() {
	// fmt.Println(unsafe.Sizeof("张三"))
	// fmt.Println(unsafe.Sizeof("李四王老五foobar"))
	s := "张三李四王老五foobar"
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	fmt.Println(sh.Len)

	// for i := 0; i < len(s); i++ {
	// 	fmt.Println(s[i])
	// }

	for _, c := range s {
		fmt.Printf("%c\n", c)
	}
}
