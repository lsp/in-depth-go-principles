package main

import "fmt"

func main() {
	var a interface{} // nil
	var p *int        // nil

	fmt.Println(a == nil)
	fmt.Println(p == nil)

	a = p // eface{ type: *int, data: nil}
	fmt.Println(a == nil)
}
