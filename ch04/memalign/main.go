// 内存对齐

package main

import (
	"fmt"
	"unsafe"
)

type S1 struct {
	a uint32
	b uint32
}

type S2 struct {
	a uint16
	b uint32
}

func main1() {
	s1 := S1{}
	s2 := S2{}

	fmt.Printf("[s1] size: %d, addr: %p\n", unsafe.Sizeof(s1), &s1)
	fmt.Printf("[s2] size: %d, addr: %p\n", unsafe.Sizeof(s2), &s2)
}

func main() {
	fmt.Printf("BOOL size: %d, align: %d\n", unsafe.Sizeof(bool(true)), unsafe.Alignof(bool(true)))
	fmt.Printf("BYTE size: %d, align: %d\n", unsafe.Sizeof(byte(0)), unsafe.Alignof(byte(0)))
	fmt.Printf("INT8 size: %d, align: %d\n", unsafe.Sizeof(int8(0)), unsafe.Alignof(int8(0)))
	fmt.Printf("INT16 size: %d, align: %d\n", unsafe.Sizeof(int16(0)), unsafe.Alignof(int16(0)))
	fmt.Printf("INT size: %d, align: %d\n", unsafe.Sizeof(int(0)), unsafe.Alignof(int(0)))
	fmt.Printf("INT32 size: %d, align: %d\n", unsafe.Sizeof(int32(0)), unsafe.Alignof(int32(0)))
	fmt.Printf("INT64 size: %d, align: %d\n", unsafe.Sizeof(int64(0)), unsafe.Alignof(int64(0)))

}
