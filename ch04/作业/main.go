package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	A int32    // 4, 4 => 8
	B []int32  // 24, 8 => 24
	C string   // 16, 8 => 16
	D bool     // 1, 1
	E struct{} // 0, 0 => 8
}

func main() {
	fmt.Println(unsafe.Sizeof(User{}), unsafe.Alignof(User{}))
}
