package main

import (
	"log"
)

type Drivable interface {
	Drive()
}

type TrafficTool interface {
	Drive()
}

type Truck struct {
	Model string
}

func (t *Truck) Drive() {
	log.Println(t.Model, "正在悠悠的耕地")
}

// Boat  船
type Boat struct {
	// Tonnage 吨位
	Tonnage int
}

func (b *Boat) Drive() {
	log.Println(b.Tonnage, "吨位的小船正在海上飘")
}

func main() {
	var c Drivable = &Truck{Model: "🚜"}
	c.Drive()

	tt, ok := c.(TrafficTool)
	if ok {
		tt.Drive()
	}

	t, ok := c.(*Truck)
	if ok {
		log.Println(t.Model)
	}

	b, ok := c.(*Boat)
	if ok {
		log.Println(b.Tonnage)
	}

	switch v := c.(type) {
	case *Boat:
		log.Println("船：", v.Tonnage)
	case *Truck:
		log.Println("卡车：", v.Model)
	case nil:
		log.Println("空：", v)
	default:
		log.Println("不知所谓：", v)
	}
}
