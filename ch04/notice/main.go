package main

import (
	"fmt"
	"time"
)

func main() {
	start := time.Now()
	ch := make(chan struct{})

	go func() {
		time.Sleep(3 * time.Second)
		ch <- struct{}{}
	}()

	<-ch
	fmt.Println("Done", time.Since(start))
}
