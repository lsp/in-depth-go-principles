# 第4章 高并发下的数据结构

## 0字节大小的变量

查看变量占用的字节数：`unsafe.Sizeof(变量)`

### 基本类型的字节数

- `int` 大小跟随系统字长

- 指针的大小也是系统字长 

### 空结构体

- 独立出现的空结构体(不被包含在其它结构体时)的地址相同，都是指向 [`zerobase`](https://github.com/golang/go/blob/54182ff54a687272dd7632c3a963e036ce03cb7c/src/runtime/malloc.go#L780)

- 空结构体主要是为了节约内存
    - 结合 map 实现 hashset
    - 结合 channel 当作纯信号

## 字符串

底层是 [`runtime.stringStruct (runtime/string.go)`](https://github.com/golang/go/blob/54182ff54a687272dd7632c3a963e036ce03cb7c/src/runtime/string.go#L238-L241)

```go
type stringStruct struct {
	str unsafe.Pointer
	len int
}
```

- `str` 指针指向底层 Byte 数组

- `len` 表示 Byte 数组的长度

> 由于 `stringStruct` 私有性，可以使用 `reflect.StringHeader` 来代替

## 切片

是对数组的引用

底层是 [`runtime.slice`](https://github.com/golang/go/blob/54182ff54a687272dd7632c3a963e036ce03cb7c/src/runtime/slice.go#L15-L19)

```go
type slice struct {
	array unsafe.Pointer
	len   int
	cap   int
}
```

⚠️ 切片扩容时，不是线程安全的，注意切片并发要加锁

## map

底层是 [`rutime.hmap`](https://github.com/golang/go/blob/54182ff54a687272dd7632c3a963e036ce03cb7c/src/runtime/map.go#L116-L130)

```go
type hmap struct {
	count     int
	flags     uint8
	B         uint8  
	noverflow uint16 
	hash0     uint32 

	buckets    unsafe.Pointer 
	oldbuckets unsafe.Pointer 
	nevacuate  uintptr        

	extra *mapextra 
}
```