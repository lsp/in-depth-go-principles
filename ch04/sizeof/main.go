package main

import (
	"fmt"
	"unsafe"
)

type K struct{}
type F struct {
	K
	Num int
}

func main() {
	// fmt.Println(unsafe.Sizeof(int(0)))
	a := K{}
	b := int(0)
	c := K{}
	d := F{}
	fmt.Printf("[a] sizeof: %d, addr: %p\n", unsafe.Sizeof(a), &a)
	fmt.Printf("[b] sizeof: %d, addr: %p\n", unsafe.Sizeof(b), &b)
	fmt.Printf("[c] sizeof: %d, addr: %p\n", unsafe.Sizeof(c), &c)
	fmt.Printf("[d] sizeof: %d, addr: %p\n", unsafe.Sizeof(d), &d)
	fmt.Printf("[d.K] sizeof: %d, addr: %p\n", unsafe.Sizeof(d.K), &d.K)
}
