package main

import "fmt"

type Set map[string]struct{}

func NewSetFromSlice(ss []string) Set {
	s := Set{}
	for _, item := range ss {
		s.Set(item)
	}
	return s
}
func (s Set) Has(item string) bool {
	if _, ok := s[item]; ok {
		return true
	}
	return false
}
func (s Set) Set(item string) {
	if s.Has(item) {
		return
	}
	s[item] = struct{}{}
}

func (s Set) Items() []string {
	items := make([]string, 0, len(s))
	for item := range s {
		items = append(items, item)
	}
	return items
}

func main() {
	ss := []string{
		"foo",
		"bar",
		"foobar",
		"foo",
		"bar",
		"foobar",
	}
	s := NewSetFromSlice(ss)
	fmt.Println(s.Items())
}
