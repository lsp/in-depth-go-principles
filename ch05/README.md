# 第5章高并发的核心工具：Goroutine 协程

## 进程、线程、协程

### 进程

- 操作系统“程序”的最小单位

- 进程用来占用内存空间

- 进程相当于厂房，占用工厂空间

### 线程

- 每个进程可以有多个线程

- 线程使用系统分配给进程的内存，线程之间共享内存

- 线程用来占用CPU时间

- 线程的调度需要由操作系统进行，开销较大

- 线程相当于工厂的生产线，占用工人的工时

- 线程里跑的程序就是生产流程

#### 线程的问题

- 线程本身占用资源大

- 线程的操作开销大

- 线程切换开销大

### 协程

- 协程就是将一段程序的运行状态打包，可以在线程之间调度

- 将生产流程打包，使得流程不固定在生产线上

- 协程并不取代线程，协程也要在线程上运行

- 线程是协程的资源，协程使用线程这个资源

#### 协程的优势

- 资源利用

- 快速调度

- 超高并发

## 协程的本质

- 协程：底层由 [`runtime.g (runtime/runtime2.go)`](https://github.com/golang/go/blob/54182ff54a687272dd7632c3a963e036ce03cb7c/src/runtime/runtime2.go#L407-L506) 实现。

- 线程：底层由 [`runtime.m (runtime/runtime2.go)`](https://github.com/golang/go/blob/54182ff54a687272dd7632c3a963e036ce03cb7c/src/runtime/runtime2.go#L519-L599) 描述

- M与G的中介 [`runtime.p (runtime/runtime2.go)`](https://github.com/golang/go/blob/54182ff54a687272dd7632c3a963e036ce03cb7c/src/runtime/runtime2.go#L601-L751)， 持有一定的G，使得M无需每次都到全局队列里获取G