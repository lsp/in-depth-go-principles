package main

import (
	"fmt"
	"time"
)

func watch(p *int) {
	for {
		if *p == 1 {
			fmt.Println("hello")
			break
		}
	}
}

func main() {
	n := 0
	go watch(&n)

	time.Sleep(time.Second)

	n = 1
	time.Sleep(time.Second)
}
