package main

import (
	"fmt"
	"time"
)

func watch(ch <-chan int) {
	if <-ch == 1 {
		fmt.Println("hello")
	}
}

func main() {
	ch := make(chan int)
	go watch(ch)

	time.Sleep(time.Second)
	ch <- 1
	time.Sleep(time.Second)
}
