# 第03章 重新认识Go语言

## 何为 Runtime？

程序的运行环境

### Go 的 Runtime 特点

- Go 没有虚拟机的概念

- Runtime 作为程序的一部分打包进二进制产物

- Runtime 随用户程序一起运行

- Runtime 与用户程序没有明显界限，直接通过函数调用

### Go 的 Runtime 能力

- 内存管理能力

- 垃圾回收能力（GC）

- 超强的并发能力（协程调度）

### Go 的 Runtime 其它特点

- 有一定的屏蔽系统调用能力

- 一些关键字其实是 Runtime 下的函数

|关键字|函数|
|----|----|
|`go`|`newproc`|
|`new`|`newobject`|
|`make`|`makeslice`, `makechain`, `makemap`...|
|`<-`|`chansend1`, `chanrecv1`|

## Go 程序是如何编译的？

### 查看编译过程

`go build -n`：不产生二进制文件，只是显示编译过程

```
词法分析 -> 句法分析 -> 语义分析 -> 中间码生成(SSA) -> 代码优化 -> 机器码生成(Plan9汇编) -> 链接
```

### 查看代码从 SSA 中间码的整个过程

```bash
# 查看 main 函数的 SSA 过程
GOSSAFUNC=main go build
```

### 查看 Plan9 汇编代码

```bash
go build -gcflags -S main.go
```

## Go 程序是如何运行的？

### Go 程序的入口

不是 `main()`，而是 [`runtime/rt0_XXX.s`](https://github.com/golang/go/blob/master/src/runtime/rt0_linux_amd64.s)