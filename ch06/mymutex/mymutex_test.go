package mymutex_test

import (
	"testing"
	"time"

	"codeberg.org/lsp/in-depth-go-principles/ch06/mymutex"
)

type Counter struct {
	mu      mymutex.MyMutex
	counter int
}

func NewCounter() *Counter {
	return &Counter{
		mu:      mymutex.NewMyMutext(),
		counter: 0,
	}
}
func (c *Counter) Incr() {
	c.mu.Lock()
	defer c.mu.UnLock()
	c.counter++
}
func (c *Counter) Count() int {
	c.mu.Lock()
	defer c.mu.UnLock()
	return c.counter
}
func TestMyMutex(t *testing.T) {
	c := NewCounter()
	for i := 0; i < 100; i++ {
		go c.Incr()
	}
	t.Log(c.Count())
	time.Sleep(time.Second)
}
