package mymutex

type MyMutex chan struct{}

func NewMyMutext() MyMutex {
	ch := make(MyMutex, 1)
	return ch
}

func (m *MyMutex) Lock() {
	(*m) <- struct{}{}
}

func (m *MyMutex) UnLock() {
	<-(*m)
}
