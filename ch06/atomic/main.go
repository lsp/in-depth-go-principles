package main

import (
	"fmt"
	"sync/atomic"
	"time"
)

func add(p *uint32) {
	// *p++
	atomic.AddUint32(p, 1)
}

func main() {
	c := uint32(0)
	for i := 0; i < 1000; i++ {
		go add(&c)
	}
	time.Sleep(1 * time.Second)
	fmt.Println(c)
}
