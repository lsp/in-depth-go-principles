# 第6章 Go高并发下的锁

## 锁的基础

### atomic 操作

- 是一种硬件层面的加锁机制

- 保证操作一个变量的时候，其它协程/线程无法访问

- 只能用于简单变量的简单操作

### sema 锁

- 信号量锁/信号锁

- 核心是一个uint32值，含义是同时可并发的数量

- 每个 sema 锁都对应一个 [`semaRoot`](https://github.com/golang/go/blob/54182ff54a687272dd7632c3a963e036ce03cb7c/src/runtime/sema.go#L40-L44) 结构体

- `semaRoot.treap`：用于协程排队的平衡二叉树

## 互斥锁 `sync.Mutex`
