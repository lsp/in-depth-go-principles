package main

import "fmt"

func a() *int {
	x := 1
	return &x
}

func main() {
	p := a()
	fmt.Println(*p)

	*p = 1000
	fmt.Println(*p)
}
